using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WebCamera : MonoBehaviour
{

	//TODO ogranize & clean
	public Text infoLabel;
		public int camTextureWidth;
		public int camTextureHeight;
		public int requestedCamFPS;
		public GameObject player;
		VRMovement playerMovement;
		public int initialFramesSize = 100;
		RawImage camCanvas = null;
		WebCamTexture camTexture = null;
		Texture2D previousFrame;
		Texture2D currentFrame;
		private float w, h; //width, height

		static double THRESHOLD = 0.1;
		private AreaManager aManager;
		private int initialFrames = 0;
		Color[] initialFrame;

	IMinigame currentMinigame;
	MinigameState currrentMinigameState = MinigameState.NoGame;

		void Start ()
		{
				playerMovement = player.GetComponent<VRMovement> ();
				aManager = new AreaManager ();
				camCanvas = gameObject.GetComponent<RawImage> ();

				WebCamDevice[] devices = WebCamTexture.devices;

				string backCamName = "";

				for (int i = 0; i < devices.Length; i++) {
						Debug.Log ("Device:" + devices [i].name + "IS FRONT FACING:" + devices [i].isFrontFacing);

						if (!devices [i].isFrontFacing) {
								backCamName = devices [i].name;
						}
				}

				camTexture = new WebCamTexture (backCamName, camTextureWidth, camTextureHeight, requestedCamFPS);

				if (camTexture.isPlaying)
						camTexture.Stop ();

				camTexture.Play ();
				previousFrame = new Texture2D (camTextureWidth, camTextureHeight);
				var colors = camTexture.GetPixels ();
				previousFrame.SetPixels (colors);
				previousFrame.Apply ();

				initialFrame = new Color[colors.Length];

				currentFrame = new Texture2D (camTextureWidth, camTextureHeight);
				w = camTextureWidth;
				h = camTextureHeight;
				aManager.addArea (new Area ((int)(w * 0.25), (int)(h * 0.75), (int)(w * 0.5), (int)(h * 0.25), new Color (0.0f, 1.0f, 0.0f), AreaLocaton.Top));
				aManager.addArea (new Area (0, (int)(h * 0.35), (int)(w * 0.15), (int)(h * 0.4), new Color (1.0f, 0.0f, 0.0f), AreaLocaton.Right));
				aManager.addArea (new Area ((int)(w * 0.85), (int)(h * 0.35), (int)(w * 0.15), (int)(h * 0.4), new Color (0.0f, 0.0f, 1.0f), AreaLocaton.Left));
				//aManager.addArea (new Area ((int)(w * 0.25), (int)(h * 0.0), (int)(w * 0.5), (int)(h * 0.25), new Color (1.0f, 1.0f, 1.0f), AreaLocaton.Bottom));
				aManager.setMovement (playerMovement);
				//camCanvas.texture = camTexture;
		}

		void Update ()
		{
				var colors = camTexture.GetPixels ();
				currentFrame.SetPixels (colors);
				currentFrame.Apply ();

		if (Input.GetKeyDown (KeyCode.Alpha1)) {
			currentMinigame = new WipeScreen(currentFrame, Color.blue);
			currrentMinigameState = MinigameState.Ongoing;
				}
		if (Input.GetKeyDown (KeyCode.Alpha2)) {
			currentMinigame = new Collect(currentFrame, Color.green, 5);
			currrentMinigameState = MinigameState.Ongoing;
		}
		if (Input.GetKeyDown (KeyCode.Alpha3)) {
			currentMinigame = new Catch(currentFrame, Color.cyan, 5);
			currrentMinigameState = MinigameState.Ongoing;
		}
		if (Input.GetKeyDown (KeyCode.Alpha4)) {
			currentMinigame = new Connect(currentFrame, Color.magenta, 5);
			currrentMinigameState = MinigameState.Ongoing;
		}
		if (Input.GetKeyDown (KeyCode.Alpha5)) {
			currentMinigame = new Avoid(currentFrame, 5);
			currrentMinigameState = MinigameState.Ongoing;
		}
		if (Input.GetKeyDown (KeyCode.Alpha6)) {
			currentMinigame = new Move(currentFrame, new Color(1.0f, 0.5f, 0));
			currrentMinigameState = MinigameState.Ongoing;
		}
		if (Input.GetKeyDown (KeyCode.Alpha7)) {
			currentMinigame = new Stay(currentFrame, Color.black);
			currrentMinigameState = MinigameState.Ongoing;
		}
		if (Input.GetKeyDown (KeyCode.Alpha8)) {
			currentMinigame = new Extinguish(currentFrame);
			currrentMinigameState = MinigameState.Ongoing;
		}
		if (Input.GetKeyDown (KeyCode.Alpha9)) {
			currentMinigame = new Push(currentFrame);
			currrentMinigameState = MinigameState.Ongoing;
		}

		if(Input.GetKeyDown(KeyCode.R))
		{
			initialFrames = 0;
			infoLabel.text = "Learning...";
		}
		
		if (initialFrames < initialFramesSize) {
						bool isBlack = true;
						for (var i = 0; i < colors.Length; i+=1) {
								if (colors [i].r > 0 || colors [i].g > 0 || colors [i].b > 0) {
										isBlack = false;
										break;
								}
						}

						if (isBlack) {
								return;
						}

						for (var i = 0; i < colors.Length; i+=1) {
								initialFrame [i].a = 1.0f;
								initialFrame [i].r += colors [i].r;
								initialFrame [i].g += colors [i].g;
								initialFrame [i].b += colors [i].b;
						}

						initialFrames++;
				} else if (initialFrames == initialFramesSize) {
			infoLabel.text = "Ready";
						for (var i = 0; i < initialFrame.Length; i+=1) {
								initialFrame [i].r /= initialFramesSize;
								initialFrame [i].g /= initialFramesSize;
								initialFrame [i].b /= initialFramesSize;
						}

						previousFrame.SetPixels (initialFrame);
						previousFrame.Apply ();
						camCanvas.texture = previousFrame;
						initialFrames++;
				} else {
			if(currrentMinigameState == MinigameState.Ongoing) {
			currrentMinigameState = currentMinigame.Update(currentFrame);
			camCanvas.texture = currentMinigame.GetMinigameTexture();
				infoLabel.text = currentMinigame.GetMessage();
			return;
			}
			else if (currrentMinigameState == MinigameState.Failed)
			{
				infoLabel.text = "FAIL!";
				return;
			}
			else if (currrentMinigameState == MinigameState.Finished)
			{
				infoLabel.text = "YAY!";
				currrentMinigameState = MinigameState.NoGame;
				initialFrames = 0;
				return;
			}
						aManager.update (previousFrame, currentFrame);
				}

				camCanvas.texture = currentFrame;

		}

		void OnDestroy ()
		{
				camTexture.Stop ();
		}

}
