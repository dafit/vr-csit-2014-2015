﻿using UnityEngine;
using System.Collections;

public class VRMovement : MonoBehaviour {

	private float moveSpeed = 6.0f;
	private float rotationSpeed = 30.0f;
	private float jumpSpeed = 500.0f;
	private int framesToJump = 0;
	
	private bool isJumping = false;

	public void RotateLeft(float speed =1.0f)
	{
		framesToJump = 0;
		Vector3 rot = transform.eulerAngles;
		transform.eulerAngles = new Vector3(rot.x, rot.y - (rotationSpeed * Time.deltaTime*speed), rot.z);
	}

	public void RotateRight(float speed =1.0f)
	{
		framesToJump = 0;
		Vector3 rot = transform.eulerAngles;
		transform.eulerAngles = new Vector3(rot.x, rot.y + (rotationSpeed * Time.deltaTime*speed), rot.z);
	}

	public void MoveForward(float speed =1.0f)
	{
		framesToJump = 0;
		transform.position += transform.forward * (moveSpeed / 10.0f) * Time.deltaTime*speed;
	}

	public void RunForward()
	{
		framesToJump = 0;
		transform.position += transform.forward * (moveSpeed) * Time.deltaTime;
	}

	public void MoveBackward()
	{
		framesToJump = 0;
		transform.position += -transform.forward * (moveSpeed / 20.0f) * Time.deltaTime;
	}

	public void Jump()
	{
		//transform.position += transform.forward * (moveSpeed) * Time.deltaTime;
		if(!isJumping)
		{
			if (framesToJump > 0)
			{
				framesToJump--;
				return;
			}

			isJumping = true;
			gameObject.rigidbody.AddForce(transform.up * jumpSpeed);
			gameObject.rigidbody.AddForce(transform.forward * jumpSpeed * 1.0f);

		}
	}

	void OnCollisionEnter(Collision col)
	{
		//TODO: poprawić by był odstęp czasowy między kolejnymi skokami
		gameObject.rigidbody.velocity = new Vector3(0, 0, 0);
		isJumping = false;
		framesToJump = 10;
	}
}
