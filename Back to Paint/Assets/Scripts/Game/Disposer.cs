﻿using UnityEngine;
using System.Collections;

public class Disposer : MonoBehaviour {

	public GameObject objToDispose;
	public float timeToDispose = 1.0f;
	public float force = 0;

	float time = 0.0f;

	// Update is called once per frame
	void Update () 
	{
		time += Time.deltaTime;

		if(time >= timeToDispose)
		{
			GameObject obj = (GameObject) GameObject.Instantiate(objToDispose);
			obj.renderer.material = renderer.material;
			obj.transform.position = transform.position + transform.forward*0.3f;
			obj.transform.rotation = transform.rotation;
			obj.rigidbody.AddForce(transform.forward * force);

			time = 0.0f;
		}
	}
}
