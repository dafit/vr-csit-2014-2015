﻿using UnityEngine;
using System.Collections;

public class CollectBall : MonoBehaviour {

	public GameObject exitPlatform;
	TrailRenderer trailRend;

	void Start()
	{
		PlayerPrefs.SetInt("hasColor", 0);
		trailRend = gameObject.GetComponent<TrailRenderer>();
	}

	void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.tag == "Ball")
		{
			PlayerPrefs.SetInt("hasColor", 1);
			gameObject.renderer.material = col.gameObject.renderer.material;
			trailRend.enabled = true;
			trailRend.material.SetColor("_TintColor", col.gameObject.renderer.material.color);
			exitPlatform.renderer.material = col.gameObject.renderer.material;
			Destroy(col.gameObject);
		}
	}
}
