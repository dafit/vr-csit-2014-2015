﻿using UnityEngine;
using System.Collections;

public class FallController : MonoBehaviour {

	public Material baseMat;
	public Material basePlatformMat;
	public GameObject exitPlatform;

	public Transform playerStartPos;
	Vector3 startPos;

	void Start()
	{
		startPos = playerStartPos.position;
	}

	void OnTriggerEnter(Collider col)
	{
		//Clear all progress
		if (col.tag == "Player") 
		{
			col.transform.position = startPos;
			col.gameObject.renderer.material = baseMat;
			col.rigidbody.velocity = Vector3.zero;
			col.transform.rotation = Quaternion.identity;

			col.GetComponent<TrailRenderer>().enabled = false;

			exitPlatform.renderer.material = basePlatformMat;
			exitPlatform.transform.rotation = Quaternion.identity;

			PlayerPrefs.SetInt("hasColor", 0);
		}
		else
		{
			Destroy(col.gameObject);
		}
	}
}
