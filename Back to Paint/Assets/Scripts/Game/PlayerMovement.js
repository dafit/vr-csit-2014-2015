#pragma strict

var moveSpeed:float = 6.0;
var turnSpeed:float = 150.0;
var jumpSpeed:float = 500.0;
var distToGround:float = 1.0;

private var isJumping = false;

function Update () 
{
	if (Input.GetButtonDown("Jump"))
	{
		if(!isJumping)
		{
			isJumping = true;
			gameObject.rigidbody.AddForce(transform.up * jumpSpeed);
		}
	}		
	
	if(Input.GetButton("Forward")){
	 	transform.position += transform.forward * moveSpeed * Time.deltaTime;
	}
	if(Input.GetButton("Backward")){
		transform.position += -transform.forward * moveSpeed * Time.deltaTime;
	}
	if(Input.GetButton("Left")){
		 transform.eulerAngles.y += -turnSpeed * Time.deltaTime;
	}
	if(Input.GetButton("Right")){
	 	transform.eulerAngles.y += turnSpeed * Time.deltaTime;
	}
}

function OnCollisionEnter(Other:Collision)
{
	isJumping = false;
}