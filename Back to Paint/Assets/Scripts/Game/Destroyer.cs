﻿using UnityEngine;
using System.Collections;

public class Destroyer : MonoBehaviour {
		
	public float timeToDestroy = 3.0f;
	float time = 0.0f;

	// Update is called once per frame
	void Update () 
	{
		time += Time.deltaTime;

		if(time >= timeToDestroy)
		{
			time = 0.0f;
			Destroy(gameObject);
		}
	}
}
