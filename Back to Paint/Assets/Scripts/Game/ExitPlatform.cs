﻿using UnityEngine;
using System.Collections;

public class ExitPlatform : MonoBehaviour {

	public string nextLevel;
	public float rotateAngle = 1;

	void Update () 
	{
		if(PlayerPrefs.GetInt("hasColor") == 1)
		{
			transform.Rotate(Vector3.up, rotateAngle);
		}
	}

	void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.tag == "Player" && PlayerPrefs.GetInt("hasColor") == 1)
		{
			Application.LoadLevel(nextLevel);
		}
	}
}
