﻿using UnityEngine;
using System.Collections;

public class Collect : IMinigame
{
		Texture2D prevFrame, currFrame;
		Color dotsColor;
		int dotsTotalNumber;
		int w, h;
		int DOT_RADIUS = 10;
		float COLOR_DIFF_THRESHOLD = 0.6f;
		IList dots;
		int framesLeft;
		Color[] square;

		public Collect (Texture2D initialFrame, Color color, int dotsTotalNumber)
		{
				square = new Color[DOT_RADIUS * 2 * DOT_RADIUS * 2];
				for (int i = 0; i<square.Length; i++) {
						square [i] = color;
				}
				framesLeft = 150;
				w = initialFrame.width;
				h = initialFrame.height;
				currFrame = new Texture2D (w, h);
				currFrame.SetPixels (initialFrame.GetPixels ());
				prevFrame = new Texture2D (w, h);
				prevFrame.SetPixels (initialFrame.GetPixels ());

				this.dotsTotalNumber = dotsTotalNumber;
				dotsColor = color;
				dots = new ArrayList ();

				for (int i = 0; i < dotsTotalNumber; i++) {
						dots.Add (new Vector2 (Random.Range (DOT_RADIUS, w - 1 - DOT_RADIUS), Random.Range (DOT_RADIUS, h - 1 - DOT_RADIUS)));
				}
				prevFrame.Apply ();
			

		}

		public MinigameState Update (Texture2D currentFrame)
		{
				Color prevColor;
				Color currentColor;
				currFrame.SetPixels (currentFrame.GetPixels ());
				for (int i = 0; i < dots.Count; i++) {
						int x = (int)((Vector2)dots [i]).x;
						int y = (int)((Vector2)dots [i]).y;
						prevColor = prevFrame.GetPixel (x, y);
						currentColor = currentFrame.GetPixel (x, y);
						float prevR = prevColor.r;
						float prevG = prevColor.g;
						float prevB = prevColor.b;
			
						float currR = currentColor.r;
						float currG = currentColor.g;
						float currB = currentColor.b;
			
						double diff = (currR - prevR) * (currR - prevR) + (currG - prevG) * (currG - prevG) + (currB - prevB) * (currB - prevB);
			
						if (diff > COLOR_DIFF_THRESHOLD) {
								dots.Remove (dots [i]);
								i--;
						} else {
								currFrame.SetPixels (x - DOT_RADIUS, y - DOT_RADIUS, DOT_RADIUS * 2, DOT_RADIUS * 2, square);
						}
			
				}
		
				prevFrame.SetPixels (currentFrame.GetPixels ());
				prevFrame.Apply ();

				currFrame.Apply ();
				framesLeft--;
				if (dots.Count <= 0) {
						return MinigameState.Finished;
				} else if (framesLeft < 0) {
						return MinigameState.Failed;
				}
				return MinigameState.Ongoing;
		}

		public Texture2D GetMinigameTexture ()
		{
				return currFrame;
		}

		public string GetMessage ()
		{
				return "Collect the dots! " + framesLeft;
		}
}
