﻿using UnityEngine;
using System.Collections;

public class Push : IMinigame
{
	
	Texture2D prevFrame, viewFrame;
	float COLOR_DIFF_THRESHOLD = 0.7f;
	int w, h;
	int top, right, bottom, left;
	Color[] abyss;
	bool topLocked, rightLocked, bottomLocked, leftLocked;
	
	public Push (Texture2D initialFrame)
	{
		w = initialFrame.width;
		h = initialFrame.height;
		prevFrame = new Texture2D (w, h);
		prevFrame.SetPixels (initialFrame.GetPixels ());
		viewFrame = new Texture2D (w, h);
		viewFrame.SetPixels (initialFrame.GetPixels ());
		abyss = new Color[viewFrame.GetPixels ().Length];
		for (int i = 0; i < abyss.Length; i++) {
			abyss[i] = Color.black;
				}
		prevFrame.Apply ();
		viewFrame.Apply ();
		top = h * 3 / 4;
		bottom = h / 4;
		right = w / 4;
		left = w * 3 / 4;
	}

	Color[] prevColor;
	Color[] currentColor;

	void updateTop ()
	{
		//TOP
		if (!topLocked) {
			if (top >= h-1) { topLocked = true; top = h-1; return; }
			prevColor = prevFrame.GetPixels (0, top, w, 1);
			currentColor = currentFrame.GetPixels (0, top, w, 1);
			
			for (int i = 0; i < currentColor.Length; i++) {
				float prevR = prevColor [i].r;
				float prevG = prevColor [i].g;
				float prevB = prevColor [i].b;
				
				float currR = currentColor [i].r;
				float currG = currentColor [i].g;
				float currB = currentColor [i].b;
				
				double diff = (currR - prevR) * (currR - prevR) + (currG - prevG) * (currG - prevG) + (currB - prevB) * (currB - prevB);
				if (diff > COLOR_DIFF_THRESHOLD) {
					top += 1;
					updateTop();
					return;
				}
			}
			top-=2;

		}
	}

	void updateBottom ()
	{
		//BOTTOM
		if (!bottomLocked) {
			if (bottom <= 0) { bottomLocked = true; bottom = 0; return; }
			prevColor = prevFrame.GetPixels (0, bottom, w, 1);
			currentColor = currentFrame.GetPixels (0, bottom, w, 1);
			
			for (int i = 0; i < currentColor.Length; i++) {
				float prevR = prevColor [i].r;
				float prevG = prevColor [i].g;
				float prevB = prevColor [i].b;
				
				float currR = currentColor [i].r;
				float currG = currentColor [i].g;
				float currB = currentColor [i].b;
				
				double diff = (currR - prevR) * (currR - prevR) + (currG - prevG) * (currG - prevG) + (currB - prevB) * (currB - prevB);
				if (diff > COLOR_DIFF_THRESHOLD) {
					bottom -= 1;
					updateBottom();
					return;
				}
			}
			bottom+=2;

		}
	}

	void updateRight ()
	{
		//RIGHT
		if (!rightLocked) {
			if (right <= 0) { rightLocked = true; right = 0; return; }
			prevColor = prevFrame.GetPixels (right, 0, 1, h);
			currentColor = currentFrame.GetPixels (right, 0, 1, h);
			
			for (int i = 0; i < currentColor.Length; i++) {
				float prevR = prevColor [i].r;
				float prevG = prevColor [i].g;
				float prevB = prevColor [i].b;
				
				float currR = currentColor [i].r;
				float currG = currentColor [i].g;
				float currB = currentColor [i].b;
				
				double diff = (currR - prevR) * (currR - prevR) + (currG - prevG) * (currG - prevG) + (currB - prevB) * (currB - prevB);
				if (diff > COLOR_DIFF_THRESHOLD) {
					right -= 1;
					updateRight ();
					return;
				}
			}
			right+=2;

		}
	}

	void updateLeft ()
	{
		//LEFT
		if (!leftLocked) {
			if (left >= w - 1) { leftLocked = true; left = w -1; return; }
			prevColor = prevFrame.GetPixels (left, 0, 1, h);
			currentColor = currentFrame.GetPixels (left, 0, 1, h);
			
			for (int i = 0; i < currentColor.Length; i++) {
				float prevR = prevColor [i].r;
				float prevG = prevColor [i].g;
				float prevB = prevColor [i].b;
				
				float currR = currentColor [i].r;
				float currG = currentColor [i].g;
				float currB = currentColor [i].b;
				
				double diff = (currR - prevR) * (currR - prevR) + (currG - prevG) * (currG - prevG) + (currB - prevB) * (currB - prevB);
				if (diff > COLOR_DIFF_THRESHOLD) {
					left += 1;
					updateLeft ();
					return;
				}
			}
			left-=2;

		}
	}

	Texture2D currentFrame;
	
	public MinigameState Update (Texture2D currentFrame)
	{
		this.currentFrame = currentFrame;
		updateTop ();
		updateBottom ();
		updateRight ();
		updateLeft ();









		if (left - right <= 1 || top - bottom <= 1)
						return MinigameState.Failed;

		prevFrame.SetPixels (currentFrame.GetPixels());
		prevFrame.Apply ();
		viewFrame.SetPixels (abyss);
		viewFrame.SetPixels (right, bottom, left - right, top - bottom, currentFrame.GetPixels (right, bottom, left - right, top - bottom));
		viewFrame.Apply ();

		if (topLocked && bottomLocked && rightLocked && leftLocked) {
			return MinigameState.Finished;
		}

		return MinigameState.Ongoing;
	}
	
	public Texture2D GetMinigameTexture ()
	{ 
		return viewFrame;
	}
	
	public string GetMessage ()
	{
		return "Push!";
	}
}
