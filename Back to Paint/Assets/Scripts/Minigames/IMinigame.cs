﻿using UnityEngine;
using System.Collections;

public enum MinigameState { NoGame, Ongoing, Finished, Failed }

public interface IMinigame {

	MinigameState Update (Texture2D currentFrame);
	Texture2D GetMinigameTexture ();
	string GetMessage ();
}
