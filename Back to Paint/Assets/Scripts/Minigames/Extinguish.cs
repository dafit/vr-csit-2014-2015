﻿using UnityEngine;
using System.Collections;

public class Extinguish : IMinigame
{
	
	Texture2D viewFrame;
	bool[] isBurned;
	float IS_BURNED_THRESHOLD = 1.0f;
	int framesLeft;
	float pixelsInFrame;
	
	public Extinguish (Texture2D initialFrame)
	{
		framesLeft = 150;
		pixelsInFrame = initialFrame.width * initialFrame.height;
		viewFrame = new Texture2D (initialFrame.width, initialFrame.height);
		viewFrame.SetPixels (initialFrame.GetPixels ());
		viewFrame.Apply ();
		isBurned = new bool[initialFrame.GetPixels ().Length];
	}
	
	public MinigameState Update (Texture2D currentFrame)
	{
		float isBurnedCounter = 0;
		Color[] currentColor = currentFrame.GetPixels ();
		Color[] viewColor = viewFrame.GetPixels ();
		for (int i = 0; i < currentColor.Length; i++) {
			if (isBurned [i]) {
				isBurnedCounter++;
				continue;
			}
			viewColor [i].r += currentColor [i].r * 0.2f;
			viewColor [i].g += currentColor [i].g * 0.2f;
			viewColor [i].b += currentColor [i].b * 0.2f;
			
			if (viewColor[i].r >= 1.0f && viewColor[i].g >= 1.0f && viewColor[i].b >= 1.0f) {
				isBurned [i] = true;
			}
			
			
		}

		viewFrame.SetPixels (viewColor);
		viewFrame.Apply ();
		framesLeft--;
		if (isBurnedCounter / pixelsInFrame >= IS_BURNED_THRESHOLD) {
			return MinigameState.Failed;
		} else if (framesLeft <= 0) {
			return MinigameState.Finished;
		}
		return MinigameState.Ongoing;
	}
	
	public Texture2D GetMinigameTexture ()
	{ 
		return viewFrame;
	}
	
	public string GetMessage ()
	{
		return "Turn off the lights! " + framesLeft;
	}
}
