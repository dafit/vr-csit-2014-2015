﻿using UnityEngine;
using System.Collections;

public class WipeScreen : IMinigame
{

		Texture2D prevFrame, viewFrame;
		Color paintColor;
		bool[] isWiped;
		float COLOR_DIFF_THRESHOLD = 0.3f;
		float IS_WIPED_THRESHOLD = 0.8f;
		int framesLeft;
		float pixelsInFrame;

		public WipeScreen (Texture2D initialFrame, Color color)
		{
				framesLeft = 150;
				pixelsInFrame = initialFrame.width * initialFrame.height;
				prevFrame = new Texture2D (initialFrame.width, initialFrame.height);
				prevFrame.SetPixels (initialFrame.GetPixels ());
				viewFrame = new Texture2D (initialFrame.width, initialFrame.height);
				viewFrame.SetPixels (initialFrame.GetPixels ());
				prevFrame.Apply ();
				viewFrame.Apply ();
				paintColor = color;
				isWiped = new bool[initialFrame.GetPixels ().Length];
		}

		public MinigameState Update (Texture2D currentFrame)
		{
				float isWipedCounter = 0;
				Color[] prevColor = prevFrame.GetPixels ();
				Color[] currentColor = currentFrame.GetPixels ();
				Color[] viewColor = viewFrame.GetPixels ();
				viewFrame.SetPixels (currentFrame.GetPixels ());
				for (int i = 0; i < currentColor.Length; i++) {
			if (isWiped [i]) {
				viewColor [i] = currentColor[i];
				isWipedCounter++;
				continue;
			}
						float prevR = prevColor [i].r;
						float prevG = prevColor [i].g;
						float prevB = prevColor [i].b;

						float currR = currentColor [i].r;
						float currG = currentColor [i].g;
						float currB = currentColor [i].b;

						double diff = (currR - prevR) * (currR - prevR) + (currG - prevG) * (currG - prevG) + (currB - prevB) * (currB - prevB);

						if (diff > COLOR_DIFF_THRESHOLD) {
								isWiped [i] = true;
						}

						if (!isWiped [i]) {
								viewColor [i] = paintColor;
						}
								
						

				}

				prevFrame.SetPixels (currentColor);
				prevFrame.Apply ();
				viewFrame.SetPixels (viewColor);
				viewFrame.Apply ();
				framesLeft--;
				if (isWipedCounter / pixelsInFrame > IS_WIPED_THRESHOLD) {
						return MinigameState.Finished;
				} else if (framesLeft < 0) {
						return MinigameState.Failed;
				}
				return MinigameState.Ongoing;
		}

		public Texture2D GetMinigameTexture ()
		{ 
				return viewFrame;
		}

		public string GetMessage ()
		{
				return "Wipe the screen! " + framesLeft;
		}
}
