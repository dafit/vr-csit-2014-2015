﻿using UnityEngine;
using System.Collections;

public class Connect : IMinigame
{
	Texture2D backgroundFrame, viewFrame;
	Color dotsColor;
	int dotPairsTotalNumber;
	int w, h;
	int DOT_RADIUS = 10;
	float COLOR_DIFF_THRESHOLD = 0.5f;
	IList dots;
	int framesLeft;
	Color[] square;
	
	public Connect (Texture2D initialFrame, Color color, int dotPairsTotalNumber)
	{
		square = new Color[DOT_RADIUS * 2 * DOT_RADIUS * 2];
		for (int i = 0; i<square.Length; i++) {
			square [i] = color;
		}
		framesLeft = 100;
		w = initialFrame.width;
		h = initialFrame.height;
		backgroundFrame = new Texture2D (w, h);
		backgroundFrame.SetPixels (initialFrame.GetPixels ());
		viewFrame = new Texture2D (w, h);
		viewFrame.SetPixels (initialFrame.GetPixels ());
		backgroundFrame.Apply ();
		this.dotPairsTotalNumber = dotPairsTotalNumber;
		dotsColor = color;
		dots = new ArrayList ();
		for (int i = 0; i < dotPairsTotalNumber; i++) {
			int x = Random.Range (DOT_RADIUS, w/2 - 1 - DOT_RADIUS);
			int y = Random.Range (DOT_RADIUS + x, h - 1 - DOT_RADIUS);
			dots.Add (new Vector2 (x, y));

			x = Random.Range (w/2 - DOT_RADIUS, w - 1 - DOT_RADIUS);
			y = Random.Range (DOT_RADIUS + (w - x), h - 1 - DOT_RADIUS);
			dots.Add (new Vector2 (x, y));
		}
		
	}
	
	public MinigameState Update (Texture2D currentFrame)
	{
		Color prevColor;
		Color currentColor;
		viewFrame.SetPixels (currentFrame.GetPixels ());
		bool[] isSquareCaptured = new bool[2];
		for (int i = 0; i < isSquareCaptured.Length; i++) {
			int x = (int)((Vector2)dots [i]).x;
			int y = (int)((Vector2)dots [i]).y;
			prevColor = backgroundFrame.GetPixel (x, y);
			currentColor = currentFrame.GetPixel (x, y);
			float prevR = prevColor.r;
			float prevG = prevColor.g;
			float prevB = prevColor.b;
			
			float currR = currentColor.r;
			float currG = currentColor.g;
			float currB = currentColor.b;
			
			double diff = (currR - prevR) * (currR - prevR) + (currG - prevG) * (currG - prevG) + (currB - prevB) * (currB - prevB);
			
			if (diff > COLOR_DIFF_THRESHOLD) {
				isSquareCaptured[i] = true;
			} else {
				isSquareCaptured[i] = false;

			}				
			viewFrame.SetPixels (x - DOT_RADIUS, y - DOT_RADIUS, DOT_RADIUS * 2, DOT_RADIUS * 2, square);
			
		}

		bool remove = true;
		for (int i = 0; i < isSquareCaptured.Length; i++) {
			if (isSquareCaptured[i] == false)
			{
				remove = false;
				break;
			}
				}

		if (remove) {
			dots.RemoveAt(0);
			dots.RemoveAt(0);
			framesLeft += 50;
				}
		viewFrame.Apply ();
		framesLeft--;
		if (dots.Count <= 0) {
			return MinigameState.Finished;
		} else if (framesLeft < 0) {
			return MinigameState.Failed;
		}
		return MinigameState.Ongoing;
	}
	
	public Texture2D GetMinigameTexture ()
	{
		return viewFrame;
	}
	
	public string GetMessage ()
	{
		return "Connect the dots! " + framesLeft;
	}
}
