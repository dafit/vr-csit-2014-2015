﻿using UnityEngine;
using System.Collections;

public class Catch : IMinigame
{
		Texture2D prevFrame, viewFrame;
		Color dotsColor;
		int dotsTotalNumber;
		int w, h;
		int DOT_RADIUS = 10;
		float COLOR_DIFF_THRESHOLD = 0.5f;
		IList dots, speeds;
		int framesLeft;
		Color[] square;
	
		public Catch (Texture2D initialFrame, Color color, int dotsTotalNumber)
		{
				square = new Color[DOT_RADIUS * 2 * DOT_RADIUS * 2];
				for (int i = 0; i<square.Length; i++) {
						square [i] = color;
				}
				framesLeft = 150;
				w = initialFrame.width;
				h = initialFrame.height;
				prevFrame = new Texture2D (w, h);
				prevFrame.SetPixels (initialFrame.GetPixels ());
				viewFrame = new Texture2D (w, h);
				viewFrame.SetPixels (initialFrame.GetPixels ());
				prevFrame.Apply ();
				this.dotsTotalNumber = dotsTotalNumber;
				dotsColor = color;
				dots = new ArrayList ();
				speeds = new ArrayList ();
				for (int i = 0; i < dotsTotalNumber; i++) {
						dots.Add (new Vector2 (Random.Range (DOT_RADIUS, w - 1 - DOT_RADIUS), Random.Range (DOT_RADIUS, h - 1 - DOT_RADIUS)));
						speeds.Add (new Vector2 (Random.Range (-8, 8), Random.Range (-8, 8)));
				}
		
		}
	
		public MinigameState Update (Texture2D currentFrame)
		{
				Color prevColor;
				Color currentColor;
				viewFrame.SetPixels (currentFrame.GetPixels ());
				for (int i = 0; i < dots.Count; i++) {
						int x = (int)((Vector2)dots [i]).x;
						int y = (int)((Vector2)dots [i]).y;
						prevColor = prevFrame.GetPixel (x, y);
						currentColor = currentFrame.GetPixel (x, y);
						float prevR = prevColor.r;
						float prevG = prevColor.g;
						float prevB = prevColor.b;
			
						float currR = currentColor.r;
						float currG = currentColor.g;
						float currB = currentColor.b;
			
						double diff = (currR - prevR) * (currR - prevR) + (currG - prevG) * (currG - prevG) + (currB - prevB) * (currB - prevB);

						if (diff > COLOR_DIFF_THRESHOLD) {
								dots.Remove (dots [i]);
								i--;
						} else {
								int oldX = x;
								int oldY = y;
				x += (int)((Vector2)speeds [i]).x;
								y += (int)((Vector2)speeds [i]).y;

								if (x < DOT_RADIUS || y < DOT_RADIUS || x > w - 1 - DOT_RADIUS || y > h - 1 - DOT_RADIUS) {

										x = oldX;
										y = oldY;

										speeds[i] = new Vector2( Random.Range (-8, 8), Random.Range (-8, 8));
								}
				dots[i] = new Vector2(x, y);

								viewFrame.SetPixels (x - DOT_RADIUS, y - DOT_RADIUS, DOT_RADIUS * 2, DOT_RADIUS * 2, square);
						}
			
				}
		
				prevFrame.SetPixels (currentFrame.GetPixels ());
				prevFrame.Apply ();
				viewFrame.Apply ();
				framesLeft--;
				if (dots.Count <= 0) {
						return MinigameState.Finished;
				} else if (framesLeft < 0) {
						return MinigameState.Failed;
				}
				return MinigameState.Ongoing;
		}
	
		public Texture2D GetMinigameTexture ()
		{
				return viewFrame;
		}
	
		public string GetMessage ()
		{
				return "Catch the dots! " + framesLeft;
		}
}
