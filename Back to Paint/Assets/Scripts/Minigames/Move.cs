﻿using UnityEngine;
using System.Collections;

public class Move : IMinigame
{
	
	Texture2D prevFrame, viewFrame;
	Color paintColor;
	float COLOR_DIFF_THRESHOLD = 0.2f;
	float framesLeft;
	float pixelsInFrame;
	
	public Move (Texture2D initialFrame, Color color)
	{
		framesLeft = 50.0f;
		pixelsInFrame = initialFrame.width * initialFrame.height;
		prevFrame = new Texture2D (initialFrame.width, initialFrame.height);
		prevFrame.SetPixels (initialFrame.GetPixels ());
		viewFrame = new Texture2D (initialFrame.width, initialFrame.height);
		viewFrame.SetPixels (initialFrame.GetPixels ());
		prevFrame.Apply ();
		viewFrame.Apply ();
		paintColor = color;
	}
	
	public MinigameState Update (Texture2D currentFrame)
	{
		float isMovedCounter = 0;
		Color[] prevColor = prevFrame.GetPixels ();
		Color[] currentColor = currentFrame.GetPixels ();
		Color[] viewColor = viewFrame.GetPixels ();
		viewFrame.SetPixels (currentFrame.GetPixels ());
		for (int i = 0; i < currentColor.Length; i++) {
			float prevR = prevColor [i].r;
			float prevG = prevColor [i].g;
			float prevB = prevColor [i].b;
			
			float currR = currentColor [i].r;
			float currG = currentColor [i].g;
			float currB = currentColor [i].b;
			
			double diff = (currR - prevR) * (currR - prevR) + (currG - prevG) * (currG - prevG) + (currB - prevB) * (currB - prevB);
			viewColor [i] = currentColor[i];
			if (diff > COLOR_DIFF_THRESHOLD) {
				isMovedCounter++;

				viewColor [i].r += paintColor.r;
				viewColor [i].g += paintColor.g;
				viewColor [i].b += paintColor.b;
			}	
		}
		
		prevFrame.SetPixels (currentColor);
		prevFrame.Apply ();
		viewFrame.SetPixels (viewColor);
		viewFrame.Apply ();
		framesLeft += isMovedCounter / pixelsInFrame * 20.0f - 0.6f;
		if (framesLeft >= 100.0f) {
			return MinigameState.Finished;
		} else if (framesLeft < 0) {
			return MinigameState.Failed;
		}
		return MinigameState.Ongoing;
	}
	
	public Texture2D GetMinigameTexture ()
	{ 
		return viewFrame;
	}
	
	public string GetMessage ()
	{
		return "MOVE! Energy: " + (int)framesLeft + "%";
	}
}
